# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Michacho@bitbucket.org/Michacho/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Michacho/stroboskop/commits/1f3750b213b5121999d9b5601ed282a238293a2b

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Michacho/stroboskop/commits/b008f82a6dc022073d35b80aa0a8d7f7fdd58841

Naloga 6.3.2:
https://bitbucket.org/Michacho/stroboskop/commits/6d2a84310082c3ed6abda44f9322b0162cdf3817

Naloga 6.3.3:
https://bitbucket.org/Michacho/stroboskop/commits/2fe0519b4e64f0b2dac345968102d53181878b86

Naloga 6.3.4:
https://bitbucket.org/Michacho/stroboskop/commits/43d0241bbefbbb3a1cd76c662067e670b950bb9a

Naloga 6.3.5:

```
git checkout master
git merge --no-ff izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Michacho/stroboskop/commits/aca00fb899bbd43209db1448f7ab68a251d1efe8

Naloga 6.4.2:
https://bitbucket.org/Michacho/stroboskop/commits/1f3f34772badd7e2dea63829ec3f455ab2a95b22

Naloga 6.4.3:
https://bitbucket.org/Michacho/stroboskop/commits/3de3ced42018ed858f4a606245cf4a9387ca2de7

Naloga 6.4.4:
https://bitbucket.org/Michacho/stroboskop/commits/017aabead5e4029672d0694c3595358ba2ab939d